import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from dataclass import RelayCmd
import logging

app = FastAPI(title="방역로봇 서비스 서버",
              description="사용자와 방역로봇의 상태를 확인하고 관리하는 기능과 사용자 인증기능을 포함하는 서버 ")
origins = ["*"]
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/command")
def set_marker_pose(relay_cmd: RelayCmd):
    print(relay_cmd)
    return relay_cmd


if __name__=="__main__":
    uvicorn.run(app, host="0.0.0.0", port=12001, log_level="info", loop="asyncio")

