import asyncio
import time

import aiohttp
import json
import threading
import requests

def send_stop_signal():
    uri = 'http://127.0.0.1:12001/command'
    data1 = {"relay1":"on"}
    data2 = {"relay1": "off"}
    threading.Thread(target=(lambda : requests.post(uri, json=data1) )).start()
    time.sleep(1)
    threading.Thread(target=(lambda : requests.post(uri, json=data2) )).start()




send_stop_signal()
