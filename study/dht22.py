import time

import Adafruit_DHT as dht
while True:
    humidity, temperature = dht.read_retry(dht.DHT22, 10)
    print(f"temp={temperature}, humidity={humidity}")
    time.sleep(1)