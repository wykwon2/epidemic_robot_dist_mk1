import io

import cv2

import ConfigManager
import SingleCamera
import calibration_util as utils
from MarkerPoseCalibration import MarkerPoseCalibration
from dataclass import MarkerInfo, MarkerPose
from logger_config import get_logger


logger = get_logger(__name__)


class MarkerDetector:
    dictionary = cv2.aruco.getPredefinedDictionary(cv2.aruco.DICT_4X4_250)
    parameters = cv2.aruco.DetectorParameters_create()

    def __init__(self, camera: SingleCamera, config: ConfigManager):
        self.camera = camera;
        self.config = config
        self.marker_pose = MarkerPoseCalibration(config)


    def get_marker_info_list(self):
        marker_info_list = []
        raw_image = self.camera.get_frame()
        if raw_image is None:
            return None
        corners, ids, rejected = cv2.aruco.detectMarkers(raw_image, self.dictionary,
                                                         parameters=self.parameters)

        if ids is None:
            return marker_info_list

        ids = ids.flatten()
        # loop over the detected ArUCo corners
        for (markerCorner, markerID) in zip(corners, ids):
            marker_info_dict = {}

            marker_info_dict['marker_id'] = markerID
            marker_info_dict['top_left'] = markerCorner[0][0]
            marker_info_dict['top_right'] = markerCorner[0][1]
            marker_info_dict['bottom_right'] = markerCorner[0][2]
            marker_info_dict['bottom_left'] = markerCorner[0][3]
            marker_info = MarkerInfo(**marker_info_dict)
            marker_info_list.append(marker_info)
        return marker_info_list

    def request_marker_image(self):
        serial = self.config.robot_serial
        addr = self.config.server_ports['ip_addr']
        port = self.config.server_ports['rest_port']

        filename = f'marker_{serial}.jpg'

        img = self.get_marker_image()
        resize_img = cv2.resize(img, (960, 540))
        cv2.imwrite(filename, resize_img)

        url = f'http://{addr}:{port}/upload-marker-image'
        files = {'data': open(filename, 'rb')}
        response = requests.post(url, files=files)
        logger.debug(f"post image={response}")


    def get_marker_image(self):
        raw_image = self.camera.get_frame()
        if raw_image is None:
            return None
        corners, ids, rejected = cv2.aruco.detectMarkers(raw_image, self.dictionary,
                                                         parameters=self.parameters)
        marker_image = utils.aruco_display(corners, ids, rejected, raw_image)
        return marker_image

    def get_marker_png_image(self):
        img = self.get_marker_image()
        if img is None:
            return None
        return cv2.imencode('.png', img)[1].tobytes()

    def get_marker_jpg_image(self):
        img = self.get_marker_image()
        if img is None:
            return None
        return cv2.imencode('.jpg', img)[1].tobytes()

    def register_marker(self, cam_id: int, marker_id: int, offset_x: float, offset_y: float, marker_length: float):
        marker_info_list = self.get_marker_info_list()
        selected_marker_id = None
        for marker_info in marker_info_list:
            if marker_info.marker_id == marker_id:
                selected_marker_id = marker_info.marker_id
                break
        if selected_marker_id is None:
            return None

        image = self.cameras.get_frame()
        self.marker_pose.register_marker(image, marker_id=marker_id, offset_x=offset_x, offset_y=offset_y,
                                         marker_length=marker_length)
        return selected_marker_id

    def get_marker_poses(self) -> [MarkerPose]:
        raw_image = self.camera.get_frame()
        return self.marker_pose.calibrate(raw_image)

    def get_nearest_marker_pose(self) -> MarkerPose:
        raw_image = self.camera.get_frame()
        # cv2.imwrite('images/camera.jpg', raw_image)
        # logger.debug("get raw image")
        return self.marker_pose.get_nearest_marker(raw_image)


if __name__ == "__main__":

    cam = SingleCamera()
    cam.start()
    marker_detector = MarkerDetector(cam)
    while True:
        logger.debug(marker_detector.get_marker_poses())
        logger.debug(marker_detector.get_nearest_marker_pose())
        image = cam.get_frame()
        # marker_detector.get_marker_image(0)
        cv2.imshow("cam0", image)
        #        cv2.imwrite('cam0.png', image)
        #         time.sleep(1)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
