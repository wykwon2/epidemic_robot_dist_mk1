import time

import cv2
import numpy as np

from dataclass import DisplayData, TASK_STATE, LIQUID, BATTERY, ERROR
from PIL import ImageFont, ImageDraw, Image

color_gray = (180, 180, 180)
color_green = (0, 176, 0)
color_yellow = (0, 255, 255)  # BGR
# color_yellow = (255, 255, 0) #RGB
color_red = (0, 0, 255)  # BGR
# color_red = (255, 0, 0) #RGB
cx = 360 * 2
cy = 218 * 2
r1 = 200 * 2  # for mk1
r2 = 180 * 2  # for mk1

# r1 = 216 * 2
# r2 = 196 * 2
offset_x = 78
offset_y = 78
cx = 360 * 2 + offset_x
cy = 212 * 2 + offset_y


# cy = 218 * 2 + offset_y

class RobotDisplay:
    image_out = None
    window_name = "ui"

    def __init__(self, data: DisplayData):

        self.img_size = (800 * 2, 480 * 2)
        self.image = Image.new('RGB', self.img_size, (0, 0, 0))
        self.power_on_img = Image.open('images/power-on.png')
        self.play_img = Image.open('images/play.png')
        self.alert_img = Image.open('images/alert.png')
        self.liquid_upper_img = Image.open('images/liquid_upper.png')
        self.liquid_lower_img = Image.open('images/liquid_lower.png')
        self.stars_img = Image.open('images/stars.png')
        self.draw = ImageDraw.Draw(self.image)
        self.task_font = ImageFont.truetype('fonts/NanumGothicBold.ttf', 40 * 2)

        # self.task_font = ImageFont.truetype('fonts/NanumGothicBold.ttf', 48 * 2)
        # self.message_font = ImageFont.truetype('fonts/NanumGothicBold.ttf', 22 * 2)
        self.message_font = ImageFont.truetype('fonts/NanumGothicBold.ttf', 20 * 2)
        self.debug_font = ImageFont.truetype('fonts/NanumGothicBold.ttf', 18 * 2)
        self.task_font_42 = ImageFont.truetype('fonts/NanumGothicBold.ttf', 42 * 2)
        self.symbol_font = ImageFont.truetype('fonts/NanumMyeongjoExtraBold.ttf', 50 * 2)
        self.set_data(data)

    def get_image(self):
        return self.image_out

    def set_data(self, data: DisplayData):
        self.image = Image.new('RGB', self.img_size, (0, 0, 0))
        self.draw = ImageDraw.Draw(self.image)
        self._draw_battery(data.battery)
        self._draw_task(data.task_status)
        self._draw_liquid(data.liquid)
        if data.task_message:
            if len(data.task_message) > 0:
                self._draw_message(data.task_message, data.debug_message)
        self.image = self.image.resize((800, 480), Image.LANCZOS)
        self.image_out = np.array(self.image)

    def _draw_battery(self, level: str):
        self.draw.ellipse((cx - r1, cy - r1, cx + r1, cy + r1), outline=color_gray, width=8 * 2)
        if level == BATTERY.high:
            self.draw.ellipse((cx - r2, cy - r2, cx + r2, cy + r2), outline=color_green, width=10 * 2)
        elif level == BATTERY.mid:
            self.draw.ellipse((cx - r2, cy - r2, cx + r2, cy + r2), outline=color_yellow, width=10 * 2)
        elif level == BATTERY.low:
            self.draw.ellipse((cx - r2, cy - r2, cx + r2, cy + r2), outline=color_red, width=10 * 2)

    def _draw_message(self, message: str, debug_message: str = None):

        self.draw.text((cx, cy + 45), message, (255, 255, 255), font=self.message_font, anchor="mm")
        if debug_message:
            self.draw.text((cx, cy + 110), f'({debug_message})', (220, 220, 220), font=self.debug_font, anchor="mm")

    def _draw_task(self, task_status):
        alert_r = 66
        alert_y_offset = 230

        if len(task_status) > 9:
            self.draw.text((cx, cy - 40 * 2), task_status, (255, 255, 255), font=self.task_font_42, anchor="mm")
        else:
            self.draw.text((cx, cy - 40 * 2), task_status, (255, 255, 255), font=self.task_font, anchor="mm")

        if task_status in (TASK_STATE.init, task_status == TASK_STATE.ready):
            self.draw.bitmap((cx - 66, cy + 130), self.power_on_img)

        elif task_status in (
        ERROR.marker, ERROR.obstacle, ERROR.unknown, ERROR.battery, ERROR.conflict, ERROR.network, ERROR.stop,
        ERROR.liquid):
            self.draw.ellipse(
                (cx - alert_r, cy + alert_y_offset - alert_r, cx + alert_r, cy + alert_y_offset + alert_r),
                fill=color_red)
            self.draw.text((cx + 3, cy + 228), '!', (255, 255, 255), font=self.symbol_font, anchor="mm")

        else:
            self.draw.bitmap((cx - 66 + 20, cy + 170), self.play_img)

    def _draw_liquid(self, liquid):
        if liquid == LIQUID.normal:
            self.draw.bitmap((cx - 30, cy - 300), self.liquid_upper_img, fill=color_green)
        elif liquid == LIQUID.low:
            self.draw.bitmap((cx - 30, cy - 300), self.liquid_upper_img, fill=color_gray)
        self.draw.bitmap((cx - 30 + 22, cy - 250), self.liquid_lower_img)

    def show_image(self):
        self.image = self.image.resize((800, 480), Image.LANCZOS)
        self.image.show()
