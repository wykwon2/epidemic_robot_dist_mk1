from dataclass import DriveParam, TurretParam, NavigationParam
from logger_config import get_logger
import json, yaml

logger = get_logger(__name__)


class ConfigManager:
    robot_serial: str
    server_ports = {}
    serial_ports = {}
    calibration_cam = {}
    drive_params: DriveParam
    turret_params: TurretParam
    navigation_params: NavigationParam

    def load_config(self, filename='conf/client_conf.yaml'):
        try:
            with open(filename, encoding="UTF8") as f:
                dict = yaml.full_load(f)
                self.server_ports = dict['[2]server_ports']
                self.serial_ports = dict['[3]serial_ports']
                self.drive_params = DriveParam(**dict['[4]drive_params'])
                self.turret_params = TurretParam(**dict['[5]turret_params'])
                self.navigation_params = NavigationParam(**dict['[6]navigation_params'])
                self.calibration_cam = dict['[7]calibration_cam']
                f.close()

                with open('conf/local_conf.yaml', encoding="UTF8") as f:
                    dict = yaml.full_load(f)
                    self.robot_serial = dict['robot_serial']
                    f.close()
        except Exception as e:
            logger.error(e)

    def save_config(self, filename='conf/client_conf.yaml'):
        try:
            dict = {}
            dict['[2]server_ports'] = self.server_ports
            dict['[3]serial_ports'] = self.serial_ports
            dict['[4]drive_params'] = self.drive_params.model_dump()
            dict['[5]turret_params'] = self.turret_params.model_dump()
            dict['[6]navigation_params'] = self.navigation_params.model_dump()
            dict['[7]calibration_cam'] = self.calibration_cam

            with open(filename, "w", encoding="UTF8") as f:
                yaml.dump(dict, f)
                f.close()
        except Exception as e:
            logger.error(e)


    def get_server_ports(self):
        return self.server_ports

    def get_serial_ports(self):
        return self.serial_ports

    def get_drive_params(self):
        return self.drive_params

    def get_turret_params(self):
        return self.turret_params

    def get_navigation_params(self):
        return self.navigation_params

    def get_calibration_cam(self):
        return self.calibration_cam

    def set_robot_serial(self, serial: str):
        self.robot_serial = serial
        self.save_config()

    def set_server_ports(self, server_ports):
        self.server_ports = server_ports
        self.save_config()

    def set_serial_ports(self, serial_ports):
        self.serial_ports = serial_ports
        self.save_config()

    def set_drive_params(self, drive_params: DriveParam):
        self.drive_params = drive_params
        self.save_config()

    def set_turret_params(self, turret_params: TurretParam):
        self.turret_params = turret_params
        self.save_config()

    def set_navigation_params(self, navigation_params: NavigationParam):
        self.navigation_params = navigation_params
        self.save_config()

    def set_calibration_cam(self, calibration_cam):
        self.calibration_cam = calibration_cam
        self.save_config()


if __name__ == "__main__":
    conf = ConfigManager()
    conf.load_config()
    logger.debug(conf.get_server_ports())
    logger.debug(conf.get_serial_ports())
    logger.debug(conf.get_drive_params())
    logger.debug(conf.get_turret_params())
    logger.debug(conf.get_navigation_params())
    logger.debug(conf.get_calibration_cam())

    conf.save_config()
