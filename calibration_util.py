import numpy as np
import cv2
import os
import datetime
import logging
logger = logging.getLogger(__name__)

def inversePerspective(rvec, tvec):
    R, _ = cv2.Rodrigues(rvec)
    R = np.matrix(R).T
    invTvec = np.dot(R, np.matrix(-tvec))
    invRvec, _ = cv2.Rodrigues(R)
    return invRvec, invTvec


def rot2eul(R):
    beta = -np.arcsin(R[2, 0])
    alpha = np.arctan2(R[2, 1] / np.cos(beta), R[2, 2] / np.cos(beta))
    gamma = np.arctan2(R[1, 0] / np.cos(beta), R[0, 0] / np.cos(beta))
    return np.array((alpha, beta, gamma))


def eul2rot(theta):
    R = np.array([[np.cos(theta[1]) * np.cos(theta[2]),
                   np.sin(theta[0]) * np.sin(theta[1]) * np.cos(theta[2]) - np.sin(theta[2]) * np.cos(theta[0]),
                   np.sin(theta[1]) * np.cos(theta[0]) * np.cos(theta[2]) + np.sin(theta[0]) * np.sin(theta[2])],
                  [np.sin(theta[2]) * np.cos(theta[1]),
                   np.sin(theta[0]) * np.sin(theta[1]) * np.sin(theta[2]) + np.cos(theta[0]) * np.cos(theta[2]),
                   np.sin(theta[1]) * np.sin(theta[2]) * np.cos(theta[0]) - np.sin(theta[0]) * np.cos(theta[2])],
                  [-np.sin(theta[1]), np.sin(theta[0]) * np.cos(theta[1]), np.cos(theta[0]) * np.cos(theta[1])]])

    return R


def Rodrigues_to_Euler(rvec):
    R, _ = cv2.Rodrigues(rvec)
    return rot2eul(R)
    """
    input
        matrix = 3x3 rotation matrix (numpy array)
        oreder(str) = rotation order of x, y, z : e.g, rotation XZY -- 'xzy'
    output
        theta1, theta2, theta3 = rotation angles in rotation order
    """


def relativePosition(rvec1, tvec1, rvec2, tvec2):
    """ Get relative position for rvec2 & tvec2. Compose the returned rvec & tvec to use composeRT with rvec2 & tvec2 """
    rvec1, tvec1 = rvec1.reshape((3, 1)), tvec1.reshape((3, 1))
    rvec2, tvec2 = rvec2.reshape((3, 1)), tvec2.reshape((3, 1))
    # Inverse the second marker
    invRvec2, invTvec2 = inversePerspective(rvec2, tvec2)
    info = cv2.composeRT(rvec1, tvec1, invRvec2, invTvec2)
    composedRvec, composedTvec = info[0], info[1]
    composedRvec = composedRvec.reshape((3, 1))
    composedTvec = composedTvec.reshape((3, 1))
    return composedRvec, composedTvec


def createFolder(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        logger.debug('Error: Creating directory. ' + directory)


def aruco_display(corners, ids, rejected, image):
    if len(corners) > 0:
        # flatten the ArUco IDs list
        ids = ids.flatten()
        # loop over the detected ArUCo corners
        for (markerCorner, markerID) in zip(corners, ids):
            # extract the marker corners (which are always returned in
            # top-left, top-right, bottom-right, and bottom-left order)
            corners = markerCorner.reshape((4, 2))
            (topLeft, topRight, bottomRight, bottomLeft) = corners
            # convert each of the (x, y)-coordinate pairs to integers
            topRight = (int(topRight[0]), int(topRight[1]))
            bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
            bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
            topLeft = (int(topLeft[0]), int(topLeft[1]))

            cv2.line(image, topLeft, topRight, (0, 255, 0), 2)
            cv2.line(image, topRight, bottomRight, (0, 255, 0), 2)
            cv2.line(image, bottomRight, bottomLeft, (0, 255, 0), 2)
            cv2.line(image, bottomLeft, topLeft, (0, 255, 0), 2)
            # compute and draw the center (x, y)-coordinates of the ArUco
            # marker
            cX = int((topLeft[0] + bottomRight[0]) / 2.0)
            cY = int((topLeft[1] + bottomRight[1]) / 2.0)
            cv2.circle(image, (cX, cY), 4, (0, 0, 255), -1)
            # draw the ArUco marker ID on the image
            cv2.putText(image, str(markerID), (topLeft[0], topLeft[1] - 10), cv2.FONT_HERSHEY_SIMPLEX,
                        0.5, (0, 255, 0), 2)
#            logger.debug(e)("[Inference] ArUco marker ID: {}".format(markerID))
        # show the output image
    return image


def save_image_timestamp(prefix: str, image):
    now = datetime.datetime.now()  # current date and time
    # logger.debug(e) (image)
    filename = "debug/{}_image_{}.jpg".format(prefix, now.strftime("%d-%b-%Y-%H-%M-%S"))
    logger.debug(filename)
    cv2.imwrite(filename, image)
    logger.debug(filename)


#
#
# def relativePosition2(rvec1, tvec1, rvec2, tvec2):
#     # case1 Inverse[T1] * T2\
#     # case2  Inverse[T2] * T1,
#     # case3 T2 * Inverse[T1]
#     # case4 T1 * Inverse[T2]
#     """ Get relative position for rvec2 & tvec2. Compose the returned rvec & tvec to use composeRT with rvec2 & tvec2 """
#     rvec1, tvec1 = rvec1.reshape((3, 1)), tvec1.reshape((3, 1))
#     rvec2, tvec2 = rvec2.reshape((3, 1)), tvec2.reshape((3, 1))
#     # Inverse the second marker
#     invRvec1, invTvec1 = inversePerspective(rvec1, tvec1)
#     invRvec2, invTvec2 = inversePerspective(rvec2, tvec2)
#     info1 = cv2.composeRT(invRvec1, invTvec1, rvec2, tvec2)
#     info2 = cv2.composeRT(invRvec2, invTvec2,rvec1, tvec1)
#     info3 = cv2.composeRT(rvec2, tvec2,invRvec1,invTvec1)
#     info4 = cv2.composeRT(rvec1, tvec1,invRvec2, invTvec2)
#
#     info = info1
#     composedRvec, composedTvec = info[0], info[1]
#     composedRvec = composedRvec.reshape((3, 1))
#     composedTvec = composedTvec.reshape((3, 1))
#
#     composedRvec1, composedTvec1 = info1[0], info1[1]
#     composedRvec1 = composedRvec1.reshape((3, 1))
#     composedTvec1 = composedTvec1.reshape((3, 1))
#
#     composedRvec2, composedTvec2 = info2[0], info2[1]
#     composedRvec2 = composedRvec2.reshape((3, 1))
#     composedTvec2 = composedTvec2.reshape((3, 1))
#
#     composedRvec3, composedTvec3 = info3[0], info3[1]
#     composedRvec3 = composedRvec3.reshape((3, 1))
#     composedTvec3 = composedTvec3.reshape((3, 1))
#
#     composedRvec4, composedTvec4 = info4[0], info4[1]
#     composedRvec4 = composedRvec4.reshape((3, 1))
#     composedTvec4 = composedTvec4.reshape((3, 1))
#     logger.debug(e)("dx1=", composedTvec1[0][0], " dy1=", composedTvec1[1][0])
#     logger.debug(e)("dx2=", composedTvec2[0][0], " dy2=", composedTvec2[1][0])
#     logger.debug(e)("dx3=", composedTvec3[0][0], " dy3=", composedTvec3[1][0])
#     logger.debug(e)("dx4=", composedTvec4[0][0], " dy4=", composedTvec4[1][0])
#     return composedRvec, composedTvec
